/* A simple server in the internet domain using TCP
   The port number is passed as an argument */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h> 
#include <sys/wait.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <time.h>

void saveFile(int);
void error(const char *msg)
{
    perror(msg);
    exit(1);
}
void *SigCatcher(int n)
{
  wait3(NULL,WNOHANG,NULL);
}

int main(int argc, char *argv[])
{
     int sockfd, newsockfd, portno, pid;
     socklen_t clilen;
     struct sockaddr_in serv_addr, cli_addr;
     int n;
     
     if (argc < 2) {
         fprintf(stderr,"ERROR, no port provided\n");
         exit(1);
     }
     
     sockfd = socket(AF_INET, SOCK_STREAM, 0);
     if (sockfd < 0) 
        error("ERROR opening socket");
     bzero((char *) &serv_addr, sizeof(serv_addr));
     portno = atoi(argv[1]);
     serv_addr.sin_family = AF_INET;
     serv_addr.sin_addr.s_addr = INADDR_ANY;
     serv_addr.sin_port = htons(portno);
     if (bind(sockfd, (struct sockaddr *) &serv_addr,
              sizeof(serv_addr)) < 0) 
              error("ERROR on binding");
     listen(sockfd,5);
     clilen = sizeof(cli_addr);
     signal(SIGCHLD,SIG_IGN);
     while (1) {
       newsockfd = accept(sockfd, 
         (struct sockaddr *) &cli_addr, &clilen);
         if (newsockfd < 0) 
             error("ERROR on accept");
         pid = fork();
         if (pid < 0)
             error("ERROR on fork");
         if (pid == 0)  {
             close(sockfd);
             saveFile(newsockfd);
             exit(0);
         }
         else close(newsockfd);
     } /* end of while */
     close(sockfd);
     return 0; 
}

void saveFile(int sock)
{
     char buffer[256];
     char filename[256];
     char newFileName[50];
     char timez[100];
     int filesize, n;
      
     bzero(buffer,256);
     
     //Read filename first
     n = read(sock,buffer,255);
     strcpy(filename, buffer);
     bzero(buffer, 256);
          
     //Add Date & Time before filename
     time_t e = time(NULL);
     struct tm tm = *localtime(&e);
     sprintf(timez, "%d%d%d-%d%d%d_", tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec);
     int x = 0;
     strncpy(newFileName, filename, x);
     newFileName[x] = '\0';
     strcat(newFileName, timez);
     strcat(newFileName, filename + x);
     
     //Read filesize
     n = read(sock, buffer, 255);
     filesize = atoi(buffer);
     char filebuffer[filesize];

     //Print new filename & read filebuffer.
     if (n < 0) error("ERROR reading from socket");
     printf("File recieved: \%s\n",newFileName);
     n = read(sock, filebuffer, filesize);
     
     //Save to file
     FILE *t = fopen(newFileName, "wb");
     fwrite(filebuffer,sizeof(filebuffer),1,t);
     fclose(t);
     
     
     n = write(sock,"File recieved!",18);
     if (n < 0) error("ERROR writing to socket");

}
