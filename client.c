#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h> 

void error(const char *msg)
{
	perror(msg);
    	exit(0);
}

int main(int argc, char *argv[])
{
    int sockfd, portno, n;
    struct sockaddr_in serv_addr;
    struct hostent *server;
	  FILE *fp;
	  char buffer[256];
    unsigned char *c = NULL;
    char filename[256];
    char file_size[10];
   	size_t size = 0;

  	if (argc < 3) {
         		fprintf(stderr,"usage %s no hostname port\n", argv[0]);
         		exit(0);
      	}
  
  	if (argc < 4) {
  		printf("No file specified\n");
  		exit(0);
  	}
  
  	portno = atoi(argv[2]);
  	sockfd = socket(AF_INET, SOCK_STREAM, 0);
    	if (sockfd < 0) 
        	error("ERROR opening socket");
    	server = gethostbyname(argv[1]);
    	if (server == NULL) {
        	fprintf(stderr,"ERROR, no such host\n");
        	exit(0);
    	}
    	bzero((char *) &serv_addr, sizeof(serv_addr));
    	serv_addr.sin_family = AF_INET;
    	bcopy((char *)server->h_addr, 
         	(char *)&serv_addr.sin_addr.s_addr,
         	server->h_length);
    	serv_addr.sin_port = htons(portno);

	if (connect(sockfd,(struct sockaddr *) &serv_addr,sizeof(serv_addr)) < 0) 
        	error("ERROR connecting");
	//	printf("Please enter the message: ");
	//    	bzero(buffer,256);

  	fp = fopen(argv[3], "rb");	
  	/* Get the buffer size */
  	fseek(fp, 0, SEEK_END); /* Go to end of file */
  	size = ftell(fp); /* How many bytes did we pass ? */	
  	/* Set position of stream to the beginning */
  	rewind(fp);	
  	/* Allocate the buffer (no need to initialize it with calloc) */
  	c = malloc((size + 1) * sizeof(*buffer)); /* size + 1 byte for the \0 */
  	/* Read the file into the buffer */
  	fread(c, size, 1, fp); /* Read 1 chunk of size bytes from fp into buffer */

  	/* NULL-terminate the buffer */
  	c[size] = '\0';

    //Write filename to server
    strcpy(filename, argv[3]);
    n = write(sockfd, filename, 18);
    
    //Write filesize to server
    sprintf(file_size,"%ld", size);
    n = write(sockfd, file_size, 10);
    
    //Write file content
  	n = write(sockfd,c,strlen(c));
   
  	if (n < 0) 
 	    error("ERROR writing to socket");
    bzero(buffer,256);
    n = read(sockfd,buffer,255);
    if (n < 0) 
       	error("ERROR reading from socket");
    printf("%s\n",buffer);
    close(sockfd);
    return 0;
}
